<?php

ini_set('memory_limit', '-1');

/**
 * Bootstrap file
 */
require 'bootstrap/bootstrap.php';

use \App\Lists\QueryElementList;

/**
 * Start time (micro)
 */
$startTime = microtime(true);

/**
 * Create the list
 */
$queryElementList = new QueryElementList();
$queryElementList->createList();

/**
 * GroupBy
 */
$queryElementList->groupBy([
    'column1' => 'sum',
    'column2' => 'avg',
    'column3' => 'avg',
    'column4' => 'max',
    'column5' => 'sum',
    'column6' => 'min',
]);

/**
 * Total element count
 */
echo 'Total element count : ' . count($queryElementList->groupedData) . '<br>';

/**
 * Get first element
 */
$firstElement = array_values($queryElementList->groupedData)[0];

/**
 * Error checking
 */
if ($firstElement['column1'] !== 800) {
    echo "ERROR IN CALCULATION for column1! -> " . $firstElement['column1'] . "<br>";
}
if ($firstElement['column2'] !== 160) {
    echo "ERROR IN CALCULATION for column2! -> " . $firstElement['column2'] . "<br>";
}
if ($firstElement['column3'] !== 180) {
    echo "ERROR IN CALCULATION for column3! -> " . $firstElement['column3'] . "<br>";
}
if ($firstElement['column4'] !== 320) {
    echo "ERROR IN CALCULATION for column4! -> " . $firstElement['column4'] . "<br>";
}
if ($firstElement['column5'] !== 780) {
    echo "ERROR IN CALCULATION for column5! -> " . $firstElement['column5'] . "<br>";
}
if ($firstElement['column6'] !== 15) {
    echo "ERROR IN CALCULATION for column6! -> " . $firstElement['column6'] . "<br>";
}

/**
 * End time (micro)
 */
$endTime = microtime(true);

/**
 * Calculate execution time
 */
$executionTime = ($endTime - $startTime);

echo 'Execution duration : ';
echo number_format((float) $executionTime, 10) . ' seconds';
