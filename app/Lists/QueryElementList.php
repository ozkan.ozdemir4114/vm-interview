<?php

namespace App\Lists;

class QueryElementList
{
    /**
     * @var int
     */
    private $seconds = 300;

    /**
     * @var array
     */
    public $elements = [];

    /**
     * @var array
     */
    public $groupedData = [];

    /**
     * Create the list
     *
     * @return array
     */
    public function createList()
    {
        /**
         * The timestamp
         */
        $timestamp = mktime(0,0,0,1,1,2020);

        /**
         * Create the list
         */
        for ($i = 1; $i < 1576800; $i++) {
            $timestamp += $this->seconds;
            //$theTime = date('d.m.Y H:i', $timestamp);

            for ($ii = 0; $ii < 400; $ii += 100) {
                $this->elements[] = [
                    'perfdate' => $timestamp,
                    'column1' => $ii + 50,
                    'column2' => $ii + 10,
                    'column3' => $ii + 30,
                    'column4' => $ii + 20,
                    'column5' => $ii + 45,
                    'column6' => $ii + 15,
                ];
            }
        }
    }

    /**
     * GroupBy
     *
     * @param array $types
     */
    public function groupBy(array $types)
    {
        foreach ($this->elements as $element) {
            if (!isset($this->groupedData[$element['perfdate']])) {
                $this->groupedData[$element['perfdate']] = [];
            }

            for ($i = 1; $i <= count($types); $i++) {
                if (isset($types['column' . $i]) && isset($element['column' . $i])) {
                    $type = $types['column' . $i];
                    $column = $element['column' . $i];

                    if (!isset($this->groupedData[$element['perfdate']]['column' . $i])) {
                        $this->groupedData[$element['perfdate']]['column' . $i] = 0;
                    }

                    switch ($type) {
                        case 'sum':
                            $this->groupedData[$element['perfdate']]['column' . $i] += $column;
                            break;
                        case 'avg':
                            if (!isset($this->groupedData[$element['perfdate']]['count-' . $i])) {
                                $this->groupedData[$element['perfdate']]['count-' . $i] = 0;
                            }
                            if (!isset($this->groupedData[$element['perfdate']]['sum-' . $i])) {
                                $this->groupedData[$element['perfdate']]['sum-' . $i] = 0;
                            }
                            $this->groupedData[$element['perfdate']]['count-' . $i] += 1;
                            $this->groupedData[$element['perfdate']]['sum-' . $i] += $column;
                            $this->groupedData[$element['perfdate']]['column' . $i] =
                                $this->groupedData[$element['perfdate']]['sum-' . $i]
                                / $this->groupedData[$element['perfdate']]['count-' . $i];
                            break;
                        case 'min':
                            $this->groupedData[$element['perfdate']]['column' . $i] =
                                $this->groupedData[$element['perfdate']]['column' . $i] !== 0
                                && $this->groupedData[$element['perfdate']]['column' . $i] < $column
                                    ? $this->groupedData[$element['perfdate']]['column' . $i] : $column;
                            break;
                        case 'max':
                            $this->groupedData[$element['perfdate']]['column' . $i] =
                                $this->groupedData[$element['perfdate']]['column' . $i] > $column
                                    ? $this->groupedData[$element['perfdate']]['column' . $i] : $column;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
    }
}
