<?php

namespace App\Models;

/**
 * Class QueryElement
 * @package App\Models
 */
class QueryElement
{
    /**
     * @var int
     */
    public int $index;

    /**
     * @var AggregateType
     */
    public AggregateType $aggregate;
}
